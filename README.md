# GitLab CI/CD with Liquibase - https://www.liquibase.org/ 

This is a repository that contains Liquibase projects (H2 Database) with GitLab CI/CD Workflow demonstrations for the following setups:
1. Liquibase Command Line Interface (CLI). 
2. Liquibase Maven commands with a Springboot app
3. Liquibase Gradle commands
4. Liquibase running in Docker
5. Liquibase running in a NodeJS Wrapper (Also please see <a href="https://www.npmjs.com/package/liquibase" target="_blank">Node.js wrapper for Liquibase</a>)

# Usage
1. Fork this repository so you can have your own copy. https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
2. From the browser go to your github "liquibasegitlabcicd" repository, and follow the instructions in the following page https://docs.gitlab.com/ee/ci/quick_start/
3. To run your job, click the "CI/CD" --> "Pipelines" --> select branch (for example: "maven_pipeline") --> "Run Pipeline".

# Notes
All Pipelines are set to be running on a Docker Images GitLab hosted runners.  Please see additional information about GitLab runners here https://docs.gitlab.com/runner/

# Adjustments
1. You can adjust your CI/CD Pipeline by checking out on the desired branch and going to the pipeline script in liquibasegitlabcicd/.gitlab-ci.yml
2. You can add more commands and flags to the pipeline script
3. For each of the projects, you will find its corresponding files in this repository.  
For example: <br />
 - For Liquibase CLI commands - H2_project folder containing a changeLog file.<br />
 - For Gradle - Gradle_h2 folder containing files like "build.gradle".<br />
 - For Maven - SalesManager_h2_version folder containing files like "pom.xml" and "application.properties" springboot class java files.<br />
 - For Docker - Docker folder containing files like a changeLog file.
 - For NodeJS - NodeJS folder containing files like index.js and other pipeline liquibase_&lt;environment&gt;.js files.
